import java.net.Socket;
import java.util.regex.*;
import java.io.*;
import java.util.Map;
import java.util.HashMap;

class ReceivingServer extends Thread{
	Socket clientSocket;
	int userEnteredId;
	String userEnteredMessage;
	Socket sendingSocket;
	HashMap<Integer,Socket> clientList;

	ReceivingServer(Socket clientSocket, HashMap<Integer,Socket>clientList){
		this.clientSocket = clientSocket;
		this.clientList = clientList;
	}

	public void run(){
		try{
			BufferedReader readStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			String str = "";
			while(!"exit".equalsIgnoreCase(str) && str!=null){
				str = readStream.readLine();
				XmlParser xmlp = new XmlParser(str);
				xmlp.parseIdAndMessage();
				userEnteredId = xmlp.getUserEnteredId();
				userEnteredMessage = xmlp.getUserEnteredMessage();
				if(clientList.containsKey(userEnteredId)){
					sendingSocket = clientList.get(userEnteredId);
					//Below Line will only be called if that particular id exist in hashmap or not.
					new SendingServer(sendingSocket, userEnteredId, userEnteredMessage).start();
				}
			}
			// readStream.close();
			// clientSocket.shutdownInput();
			// clientSocket.shutdownOutput();
			// clientSocket.close();
		}catch(Exception e){
			System.out.println("Exception caught in ReceivingServer: "+e);
		}
	}
}