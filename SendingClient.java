import java.io.*;
import java.net.*;

class SendingClient extends Thread{
	Socket s;
	SendingClient(Socket s){
		this.s = s;
	}

	public void run(){
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter writeStream = new PrintWriter(s.getOutputStream(), true);

			String input = "";

			while(!"exit".equalsIgnoreCase(input)){
				input = br.readLine();
				if(input!=null){
					writeStream.println(input);
				}
			}
			s.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}
}