import java.io.BufferedReader;
import java.util.concurrent.ThreadLocalRandom;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;

public class ServerCode{
	public static void main(String[] args) throws IOException{
		ServerSocket s = new ServerSocket(8765);
		Socket clientSocket;
		int id;
		ListOfClients ls = new ListOfClients();
		HashMap<Integer,Socket> clientList = ls.getClientsList();
		while(true){
			clientSocket = s.accept();
			id = ServerCode.generateId();
			ls.addClient(id,clientSocket);
			System.out.println(ls.getClientsList());
			new ReceivingServer(clientSocket,clientList).start();
		}
	}
	public static int generateId(){
		int uniqueId =ThreadLocalRandom.current().nextInt(000, 1000);
		return uniqueId;
	}
}