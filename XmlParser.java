import java.util.regex.*;

public class XmlParser{
	String str;
	int userEnteredId;
	String userEnteredMessage;
	String uniqueIdRegex = "(^To:)(\\d{3,3})( M:)(.+)";
	public XmlParser(String str){
		this.str = str;
	}
	public void parseIdAndMessage(){
		Pattern uniqueIdPattern= Pattern.compile(uniqueIdRegex);
		Matcher uniqueIdMatcher= uniqueIdPattern.matcher(str);
		if(uniqueIdMatcher.matches()){
			MatchResult mr = uniqueIdMatcher.toMatchResult();
			userEnteredId = Integer.parseInt(mr.group(2));
			userEnteredMessage = mr.group(4);
		}
	}
	public int getUserEnteredId(){
		return this.userEnteredId;
	}
	public String getUserEnteredMessage(){
		return this.userEnteredMessage;
	}
}