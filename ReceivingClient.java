import java.io.*;
import java.net.*;

class ReceivingClient extends Thread{
	Socket s;
	ReceivingClient(Socket s){
		this.s = s;
	}

	public void run(){
		try{
			BufferedReader readStream = new BufferedReader(new InputStreamReader(s.getInputStream()));

			String str = "";
			while(!"exit".equalsIgnoreCase(str)){
				str = readStream.readLine();
				if(str!=null){
					System.out.println(str);
				}
			}
			readStream.close();
			s.close();
			// Thread.stop();
		}catch (Exception e) {
			System.out.println(e);
		}
	}
}