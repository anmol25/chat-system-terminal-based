import java.io.*;
import java.net.*;

public class ClientCode{
	public static void main(String[] args) throws IOException{
		Socket s = new Socket("127.0.0.1", 8765);
		new SendingClient(s).start();
		new ReceivingClient(s).start();
	}
}