import java.net.Socket;
import java.io.*;

class SendingServer extends Thread{
	Socket clientSocket;
	int userEnteredId;
	String userEnteredMessage;
	SendingServer(Socket clientSocket, int userEnteredId, String userEnteredMessage){
		this.clientSocket = clientSocket;
		this.userEnteredId = userEnteredId;
		this.userEnteredMessage = userEnteredMessage;
	}

	public void run(){
		try{
			PrintWriter writeStream = new PrintWriter(clientSocket.getOutputStream(), true);
			writeStream.println(userEnteredMessage);
			
			// BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			// String input = "";
			// writeStream.println("Unique Id assigned to you is: "+this.id);
			// while(!"exit".equalsIgnoreCase(input)){
			// 	input = br.readLine();
			// 	writeStream.println("From Server: "+input);
			// }
			// br.close();
			// writeStream.close();
			// clientSocket.shutdownInput();
			// clientSocket.shutdownOutput();
			// System.exit(0);
			// clientSocket.close();
		}catch(Exception e){
			System.out.println("Exception caught in SendingServer: "+e);
		}
	}
}