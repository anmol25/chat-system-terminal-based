import java.util.Map;
import java.util.HashMap;
import java.net.Socket;

public class ListOfClients{
	int id;
	Socket clientSocket;

	HashMap<Integer,Socket> clientList = new HashMap<>();

	public void addClient(int id, Socket clientSocket){
		this.id = id;
		this.clientSocket = clientSocket;
		if(!clientList.containsKey(this.id)){
			clientList.put(this.id,this.clientSocket);
		}
	}

	public void removeClient(int id){
		clientList.remove(id);
	}

	public HashMap<Integer,Socket> getClientsList(){
		return this.clientList;
	}
}